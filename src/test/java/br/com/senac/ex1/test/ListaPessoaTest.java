/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex1.test;

import br.com.senac.ListaPessoa;
import br.com.senac.ex1.model.Pessoa;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class ListaPessoaTest {
    
    public ListaPessoaTest() {
    }
    
    
    @Test
    public void joaoDeveSeroMaisNovo(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa joao = new Pessoa("Josão", 10);
        Pessoa miguel = new Pessoa("Miguel", 15);
        Pessoa malaquias = new Pessoa("Malaquias", 20);
        lista.add(joao);
        lista.add(miguel);
        lista.add(malaquias);
        
        Pessoa pessoaMaisNova = ListaPessoa.getPessoaMaisNova(lista);
        assertEquals(joao, pessoaMaisNova);
    }
    
     public void miguelNaoDeveSeroMaisNovo(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa joao = new Pessoa("Josão", 10);
        Pessoa miguel = new Pessoa("Miguel", 15);
        Pessoa malaquias = new Pessoa("Malaquias", 20);
        lista.add(joao);
        lista.add(miguel);
        lista.add(malaquias);
        
        Pessoa pessoaMaisNova = ListaPessoa.getPessoaMaisNova(lista);
        assertNotEquals(joao, pessoaMaisNova);
    }
}
